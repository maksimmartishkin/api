<?php

//$params = require __DIR__ . '/params.php';
//$db = require __DIR__ . '/db.php';




$config = [
	'id' => 'micro-app',
	// basePath (базовый путь) приложения будет каталог `micro-app`
	'basePath' => dirname(__DIR__),
	// это пространство имен где приложение будет искать все контроллеры
//	'controllerNamespace' => 'micro\controllers',
	// установим псевдоним '@micro', чтобы включить автозагрузку классов из пространства имен 'micro'
	'aliases' => [
//		'@micro' => __DIR__,
	],
	'components' => [
//		'db' => [
//			'class' => 'yii\db\Connection',
//			'dsn' => 'sqlite:@micro/database.sqlite',
//		],
		'request' => [
			'parsers' => [
				'application/json' => 'yii\web\JsonParser',
			]
		],
		'urlManager' => [
			'enablePrettyUrl' => true,
			'enableStrictParsing' => true,
			'showScriptName' => false,
			'rules' => [
				['class' => 'yii\rest\UrlRule', 'controller' => 'user'],
			],
		]
	],
];










//$config = [
//    'id' => 'basic',
//    'basePath' => dirname(__DIR__),
////    'bootstrap' => ['log'],
//	'controllerNamespace' => 'micro\controllers',
//    'aliases' => [
////        '@bower' => '@vendor/bower-asset',
////        '@npm'   => '@vendor/npm-asset',
//		'@micro' => __DIR__,
//    ],
//    'components' => [
//        'request' => [
//            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
////            'cookieValidationKey' => '234rwerfsnwkadsfuiwporesfdlkjl',
//        ],
//        'cache' => [
//            'class' => 'yii\caching\FileCache',
//        ],
//        'user' => [
//            'identityClass' => 'app\models\User',
//            'enableAutoLogin' => true,
//        ],
//        'errorHandler' => [
//            'errorAction' => 'site/error',
//        ],
//        'mailer' => [
//            'class' => 'yii\swiftmailer\Mailer',
//            // send all mails to a file by default. You have to set
//            // 'useFileTransport' to false and configure a transport
//            // for the mailer to send real emails.
//            'useFileTransport' => true,
//        ],
//        'log' => [
//            'traceLevel' => YII_DEBUG ? 3 : 0,
//            'targets' => [
//                [
//                    'class' => 'yii\log\FileTarget',
//                    'levels' => ['error', 'warning'],
//                ],
//            ],
//        ],
//        'db' => $db,
/*
'urlManager' => [
	'enablePrettyUrl' => true,
	'showScriptName' => false,
	'rules' => [
	],
],
*/
//    ],
//    'params' => $params,
//];

if (YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][] = 'debug';
	$config['modules']['debug'] = [
		'class' => 'yii\debug\Module',
		// uncomment the following to add your IP if you are not connecting from localhost.
		//'allowedIPs' => ['127.0.0.1', '::1'],
	];

	$config['bootstrap'][] = 'gii';
	$config['modules']['gii'] = [
		'class' => 'yii\gii\Module',
		// uncomment the following to add your IP if you are not connecting from localhost.
		//'allowedIPs' => ['127.0.0.1', '::1'],
	];
}

return $config;
